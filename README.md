# slither DOT - retro slither game enclosed in an wood design #
* https://embeto.com/slither-dot/
* custom-made retro slither game
* hardware based on ATMega168
* long-lasting up to 8h on-board rechargable battery
* on board charging circuit for Li-Ion battery
* on board battery level indicator and leds meter display
* non-volatile high score memory
* high-speed option  

## Repository Structure ##

* documents 
	* datasheets 
	* schematics - each tapeout version is listed here in a directory containing the corresponding BOM and PDF schematic
	* drawings - white blueprints from 3D software 

* pcbDesign 
	* LEDsPCB
		* schematics - Working directory for LEDsPCB circuit board
		* tapeouts (see *circuit Board design* section for more)
	* mainPCB
		* schematics - Working directory for mainPCB circuit board
		* tapeouts (see *circuit Board design* section for more)
	* switchesPCB
		* there are here 2 variants, one _LEFT and one _RIGHT 
		* schematics - Working directory for switchesPCB
		* tapeouts (see *circuit Board design* section for more) - every PCB tapeout is listed here in a directory containing the exact schematic and gerber files

* software 
	* arduino - sketches used in the early stages of design for research and development 
	* cpp - eclipse cpp project for atmel implementation
	
## Circuit Board design ##

* LEDsPCB
	* v1.0 - (factory produced) - all good, a DIP LED battery meter could have been used, green LEDs for level were replaced with blue ones

* mainPCB
	* v1.0 - (factory produced) - major error, VCC and GND pins mismatched, issue resolved manually on PCB with some wire jumpers

* switchesPCB 
	* v1.0 - (factory produced) - all good

## 3D design ##

* view/download the 3D enclosure design in Web Browser [here](https://a360.co/2IYAgEc)