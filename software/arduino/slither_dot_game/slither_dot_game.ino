#include<font_8x4.h>
#include<font_5x4.h>
#include<EEPROM.h>

typedef struct coordonate
{
  char x,y;
};

//Prototipuri de functii aici
void setup(void);
void Print(void);
void LEDArrayInitialize(void);
void InitializeSnake(void);
void LEDArrayRefresh(void);
bool MoveSnake(void);
void RefreshDirection(bool);
void ReadButtons(void);
bool isVal();
void GeneratePoint(void);
bool ValidateSnake(void);
void ConvertText(bool);
void ScrollText(unsigned int);
void DisplayText(void);
void AddressWrite(unsigned char address);
void CommandWrite(unsigned int command);
void SetHT1632CAs3208(void);

#define CS PD0
#define WR PD1
#define DATA PD3
#define RD PD2
#define uTIME 50

#define RC_MASTER_MODE 0b100000110000
#define SYS_DIS        0b100000000000
#define SYS_EN         0b100000000010
#define LED_OFF        0b100000000010
#define LED_ON         0b100000000110
#define N_MOS_COM8     0b100001000000
#define PWM_16         0b100101011110

//#define uri aici
#define MAXLENG 65
#define PORT_R A3
#define PORT_L A0
#define PORT_OK1 A2
#define PORT_OK2 A1
#define INITIAL_MARGIN 7400
#define EXTRA_MARGIN 5400
#define FAST_MARGIN 3950
#define SPEED 80

//variabile globale
unsigned char LEDArray[32];
char dir=0;
coordonate snake[MAXLENG];
coordonate punct;
bool last_LState=0;
bool last_RState=0;
bool schimbare=0;
unsigned char SNLEG=8;
unsigned char converted[200],m_length=0;
String text;
unsigned long int scor=100;
int MARGIN;
//definiri de functii

void AddressWrite(unsigned char address)
{
  unsigned char i, temp;
  address=address&0x7F;
  
  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,1);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,0);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,1);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  for(i=0;i<7;i++)
  {
    digitalWrite(WR,0);
    delayMicroseconds(uTIME);  
    temp=address&0x40;
    address=address<<1;
    temp=temp>>6;
    digitalWrite(DATA,temp);
    delayMicroseconds(uTIME);  
    digitalWrite(WR,1);
    delayMicroseconds(uTIME);  
    
  }

  
}

void CommandWrite(unsigned int command)
{
  unsigned char i;
  unsigned int j;
  command=command&0x0FFF;//12 bit comm word
  digitalWrite(CS,1);//cs is OFF
  delayMicroseconds(uTIME);
  digitalWrite(CS,0);//cs is OFF
  delayMicroseconds(uTIME);
  for(i=0;i<12;i++)
  {
    digitalWrite(WR,0);
    delayMicroseconds(uTIME);
    
    j=command&0x0800;//return the mMSB
    command=command<<1;
    j=j>>11;
    digitalWrite(DATA,j);
    delayMicroseconds(uTIME);
    
    digitalWrite(WR,1);
    delayMicroseconds(uTIME);
    
  }
  digitalWrite(CS,1);//cs is OFF
}

void SetHT1632CAs3208(void)
{
  CommandWrite(SYS_EN);
  CommandWrite(LED_ON);
  CommandWrite(RC_MASTER_MODE);
  CommandWrite(N_MOS_COM8);
  CommandWrite(PWM_16);
}



void LEDArrayInitialize(void)
{
  for(int i=0;i<32;i++)
    LEDArray[i]=0x00;
}
void LEDArrayRefresh(void)
{
    for(int i=0;i<SNLEG;i++)
      LEDArray[snake[i].x]|=1<<(snake[i].y);
    LEDArray[punct.x]|=(1<<punct.y);
}
void InitializeSnake(void)
{
  
  for(int i=0;i<8;i++)
  {
    snake[i].x=i+1;
    snake[i].y=4;
  }
}
void ConvertText(bool p)//1-> 5*4 FONT   0->8*4 FONT
{
  for(int i=0;i<200;i++)
    converted[i]=0;
  if(p)
  {
     for(int i=0;i<text.length();i++)
      {
        text[i]=text[i]-0x20;
        if(text[i]==0x00)
        {
          converted[m_length]=FONT_5X4[0];
          m_length=m_length+2;
        }
        else
        {
          for(int j=FONT_5X4_END[text[i]-1];j<FONT_5X4_END[text[i]];j++)
          {
            converted[m_length]=FONT_5X4[j]>>2;
            m_length=m_length+1;
          }
          m_length=m_length+1;
        }   
      }
  }
  else
  {
    for(int i=0;i<text.length();i++)
      {
        text[i]=text[i]-0x20;
        if(text[i]==0x00)
        {
          converted[m_length]=FONT_8X4[0];
          m_length=m_length+2;
        }
        else
        {
          for(int j=FONT_8X4_END[text[i]-1];j<FONT_8X4_END[text[i]];j++)
          {
            converted[m_length]=FONT_8X4[j];
            m_length=m_length+1;
          }   
          m_length=m_length+1;
        }
      }
  }
}
bool MoveSnake(void)
{
  //ce facem cu noul punct de snake?
  coordonate new_point=snake[SNLEG-1];
  if(dir==0)
    new_point.x=snake[SNLEG-1].x+1;
  if(dir==2)
    new_point.x=snake[SNLEG-1].x-1;
  if(dir==1)
    new_point.y=snake[SNLEG-1].y-1;
  if(dir==3)
    new_point.y=snake[SNLEG-1].y+1;
  
    
  //validarea datelor
  if(new_point.x<0)
    new_point.x=31;
  if(new_point.y<0)
    new_point.y=7;
  if(new_point.x>31)
    new_point.x=0;
  if(new_point.y>7)
    new_point.y=0;

  if((new_point.x==punct.x)&&(new_point.y==punct.y))
  {
    SNLEG++;
    snake[SNLEG-1]=punct;
    return 1;
  }
  else
  {
    for(int i=0;i<SNLEG-1;i++)//move tail dummy 
      snake[i]=snake[i+1];
    snake[SNLEG-1]=new_point;
    return 0;
  }


}

void Print(void)
{
  unsigned char i=0,j=0;
  bool data;
  AddressWrite(0x00);
  for(i=0;i<32;i++)
  {
     for(j=0;j<8;j++)
     {
          data=(LEDArray[i]&(1<<(7-j)))>>(7-j);
          digitalWrite(WR,0);
          delayMicroseconds(uTIME);
          digitalWrite(DATA,data);
          delayMicroseconds(uTIME);
          digitalWrite(WR,1);
          delayMicroseconds(uTIME);
     }
  }
}

void RefreshDirection(bool p)
{
  if(p)
    dir=(dir+1)%4;
  else
    if((dir-1)>=0)
      dir=dir-1;
    else
      dir=3;
}
void ReadButtons(void)
{
  bool LState=!digitalRead(PORT_L);
  bool RState=!digitalRead(PORT_R);
  bool p=0;
  if(LState&(LState^last_LState))
  {
    RefreshDirection(0);
    schimbare=1;
  }
  if(RState&(RState^last_RState))
  {
    RefreshDirection(1);
    schimbare=1;
  }
  last_LState=LState;
  last_RState=RState;

  
}
bool isVal()
{
  for(int i=0;i<SNLEG;i++)
  {
    if(!abs(snake[i].x-punct.x))
      return 0;
    if(!abs(snake[i].y-punct.y))
      return 0;
  }
  return 1;
}


void GeneratePoint(void)
{
  char c=0;
  do
  {
    punct.x=abs((rand()+c)%32);
    punct.y=abs((rand()+c)%8);
    c++;
  }while(!isVal());
  punct.x=abs(punct.x);
  punct.y=abs(punct.y);
}
bool ValidateSnake(void)
{
  for(int i=0;i<SNLEG-2;i++)
    if((snake[i].x==snake[SNLEG-1].x)&&(snake[i].y==snake[SNLEG-1].y))
      return 0;
  return 1;
}
void ScrollText(unsigned int sp)
{
  for(int first=0;first<m_length;first++)
  {
    LEDArrayInitialize();
    for(int i=first;i<32+first;i++)
      LEDArray[i-first]=converted[i];
    digitalWrite(CS,0);
    Print();
    digitalWrite(CS,1);
    delay(sp*SPEED);
  }
}
void DisplayText(void)//doar afiseaza textul si nu il scrolluieste !!
{
  LEDArrayInitialize();
  for(int i=0;i<32;i++)
    LEDArray[i]=converted[i];
  digitalWrite(CS,0);
  Print();
  digitalWrite(CS,1);
}
void setup(void) 
{
  pinMode(CS, OUTPUT);
  pinMode(WR, OUTPUT);
  pinMode(DATA, OUTPUT);

  pinMode(PORT_L, INPUT_PULLUP);
  pinMode(PORT_R, INPUT_PULLUP);
  pinMode(PORT_OK1, INPUT_PULLUP);
  pinMode(PORT_OK2, INPUT_PULLUP);

  SetHT1632CAs3208();
  
  m_length=0;
  text="SLITHER";
  ConvertText(0);
  DisplayText();
  delay(10);
  ScrollText(1);
  
  m_length=0;
  text=EEPROM.read(0x00);
  text+=" TRY?";
  ConvertText(0);
  DisplayText();
  while(!(!digitalRead(PORT_OK1)|!digitalRead(PORT_OK2)))
    if((!digitalRead(PORT_R))&&(!digitalRead(PORT_L)))
      EEPROM.write(0x00, 0x00);

  
  
}

unsigned long int count=0,count_fast=0;
int p=0;

void loop(void) 
{
  
  //asteapta ok to begin
  m_length=0;
  text=EEPROM.read(0x00);
  text+=" TRY?";
  ConvertText(0);
  DisplayText();
  while(!(!digitalRead(PORT_OK1)|!digitalRead(PORT_OK2)))
    if((!digitalRead(PORT_R))&&(!digitalRead(PORT_L)))
      EEPROM.write(0x00, 0x00);
  MARGIN=INITIAL_MARGIN;
  bool r=0;
  schimbare=0;
  dir=0;
  SNLEG=8;
  InitializeSnake();
  LEDArrayInitialize();
  LEDArrayRefresh();
  GeneratePoint();
  scor=millis();
  while(1)
  {
   if(!schimbare)
    ReadButtons();

    if(!digitalRead(PORT_OK1)|!digitalRead(PORT_OK2))
      r=1;
    else
      r=0;
    if(count>=(MARGIN-r*EXTRA_MARGIN))
    {
      
      if(MoveSnake())
      {
        GeneratePoint();
        if(MARGIN>=FAST_MARGIN)
          MARGIN-=50;
      }
      schimbare=0;
      if(!ValidateSnake())
      {
        bool q=0;
        for(int i=0;i<12;i++)
        {
          LEDArrayInitialize();
          if(q)
            LEDArrayRefresh();
          q^=1;
          digitalWrite(CS,0);
          Print();
          digitalWrite(CS,1);
          delay(210);
        }
        scor=millis()-scor;
        break;
      }
      if(SNLEG==MAXLENG)
      {
         scor=millis()-scor;
         break;
      } 

      LEDArrayInitialize();
      LEDArrayRefresh();
      digitalWrite(CS,0);
      Print();
      digitalWrite(CS,1);
      count=0;  
    }
    
    if(count_fast>=FAST_MARGIN)
    {
      LEDArray[punct.x]^=1<<punct.y;
      digitalWrite(CS,0);
      Print();
      digitalWrite(CS,1);
      
      count_fast=0;
    }
    
    count++;
    count_fast++;
  }
    
    m_length=0;
    if (SNLEG>EEPROM.read(0x00))
    {  
      text="HIGH SCORE!";
      EEPROM.write(0x00, byte(SNLEG));
    }
    else
    {
      text="GAME OVER!";
    }
      
    ConvertText(0);
    DisplayText();
    delay(500);  //text us too long, no need to stay on the screen too much
    ScrollText(1);

  //afiseaza mesaj game over sau scor
  
  //afiseaza mesaj daca vrei sa joci din nou

}
