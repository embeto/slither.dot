//#define uri aici
#define CS 4
#define WR 8
#define DATA 9
#define RD 10

#define RC_MASTER_MODE 0b100000110000
#define SYS_DIS        0b100000000000
#define SYS_EN         0b100000000010
#define LED_OFF        0b100000000010
#define LED_ON         0b100000000110
#define N_MOS_COM8     0b100001000000
#define PWM_16         0b100101011110

#define uTIME 10//in us

//Prototipuri de functii aici
void setup(void);
void SetHT1632CAs3208(void);
void CommandWrite(unsigned int);
void Print(void);
void AddressWrite(unsigned char);
void LEDArrayInitialize(void);

//variabile globale
unsigned char LEDArray[32];


//definiri de functii
void LEDArrayInitialize(void)
{
  for(int i=0;i<32;i++)
    LEDArray[i]=0x00;
   
}
void SetHT1632CAs3208(void)
{
  CommandWrite(SYS_EN);
  CommandWrite(LED_ON);
  CommandWrite(RC_MASTER_MODE);
  CommandWrite(N_MOS_COM8);
  CommandWrite(PWM_16);
}
void CommandWrite(unsigned int command)
{
  unsigned char i;
  unsigned int j;
  command=command&0x0FFF;//12 bit comm word
  digitalWrite(CS,1);//cs is OFF
  delayMicroseconds(uTIME);
  digitalWrite(CS,0);//cs is OFF
  delayMicroseconds(uTIME);
  for(i=0;i<12;i++)
  {
    digitalWrite(WR,0);
    delayMicroseconds(uTIME);
    
    j=command&0x0800;//return the mMSB
    command=command<<1;
    j=j>>11;
    digitalWrite(DATA,j);
    delayMicroseconds(uTIME);
    
    digitalWrite(WR,1);
    delayMicroseconds(uTIME);
    
  }
  digitalWrite(CS,1);//cs is OFF
}
void AddressWrite(unsigned char address)
{
  unsigned char i, temp;
  address=address&0x7F;
  
  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,1);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,0);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,1);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  for(i=0;i<7;i++)
  {
    digitalWrite(WR,0);
    delayMicroseconds(uTIME);  
    temp=address&0x40;
    address=address<<1;
    temp=temp>>6;
    digitalWrite(DATA,temp);
    delayMicroseconds(uTIME);  
    digitalWrite(WR,1);
    delayMicroseconds(uTIME);  
    
  }

  
}
void Print(void)
{
  unsigned char i=0,j=0;
  bool data;
  AddressWrite(0x00);
  for(i=0;i<32;i++)
  {
     for(j=0;j<8;j++)
     {
          data=(LEDArray[i]&(1<<j))>>j;
          digitalWrite(WR,0);
          delayMicroseconds(uTIME);
          digitalWrite(DATA,data);
          delayMicroseconds(uTIME);
          digitalWrite(WR,1);
          delayMicroseconds(uTIME);
     }
  }
}

void setup(void) 
{
  LEDArrayInitialize();
  pinMode(CS, OUTPUT);
  pinMode(WR, OUTPUT);
  pinMode(DATA, OUTPUT);
  pinMode(13,OUTPUT);
  pinMode(A0, INPUT);
  digitalWrite(13,HIGH);
  SetHT1632CAs3208();

  
  
}
unsigned char count=0;
void loop() 
{

  if(count==8)
  {
    count=0;
    LEDArrayInitialize();
  }
  else
  {
    count++;
    for(int i=0;i<32;i++)
      LEDArray[i]|=(LEDArray[i]<<1)+1;
  }
  digitalWrite(CS,0);
  Print();
  digitalWrite(CS,1);
  delay(1124-analogRead(A0));
  
}
