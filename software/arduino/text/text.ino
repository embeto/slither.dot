//#define uri aici
#define CS 4
#define WR 8
#define DATA 9
#define RD 10

#define RC_MASTER_MODE 0b100000110000
#define SYS_DIS        0b100000000000
#define SYS_EN         0b100000000010
#define LED_OFF        0b100000000010
#define LED_ON         0b100000000110
#define N_MOS_COM8     0b100001000000
#define PWM_16         0b100101011110

#define uTIME 10//in us

int FONT[] = 
{
  0b00000000,                                                   // SPACE0x00
  0b01110000, 0b11111101, 0b01110000,                           // !    0x21 1
  0b11000000, 0b00000000, 0b11000000,                           // "    0x22 2
  0b00100100, 0b11111111, 0b00100100, 0b11111111, 0b00100100,   // #    0x23 3
  0b00100100, 0b01010010, 0b11011011, 0b01001010, 0b00100100,   // $    0x24 4
  0b11000001, 0b11000110, 0b00011000, 0b01100011, 0b10000011,   // %    0x25 5
  0b01101110, 0b10010001, 0b10010010, 0b01100101,               // &    0x26 6
  0b11000000,                                                   // '    0x27 7
  0b00111100, 0b01000010, 0b10000001,                           // (    0x28
  0b10000001, 0b01000010, 0b00111100,                           // )    0x29
  0b10100000, 0b01000000, 0b10100000,                           // *    0x2A
  0b00001000, 0b00001000, 0b00111110, 0b00001000, 0b00001000,   // +    0x2B
  0b00000001, 0b00000010,                                       // ,    0x2C
  0b00001000, 0b00001000, 0b00001000,                           // -    0x2D
  0b00000001,                                                   // .    0x2E
  0b00000011, 0b00001100, 0b00110000, 0b11000000,               // /    0x2F
  0b01111110, 0b10110001, 0b10001101, 0b01111110,               // 0    0x30
  0b01000001, 0b11111111, 0b00000001,                           // 1    0x31
  0b01000011, 0b10000101, 0b10001001, 0b01110001,               // 2    0x32
  0b01000010, 0b10001001, 0b10001001, 0b01110110,               // 3    0x33
  0b00011100, 0b00100100, 0b01001111, 0b10000100,               // 4    0x34
  0b11110001, 0b10010001, 0b10010001, 0b10001110,               // 5    0x35
  0b01111110, 0b10001001, 0b10001001, 0b01000110,               // 6    0x36
  0b10000000, 0b10000111, 0b10011000, 0b11100000,               // 7    0x37
  0b01110110, 0b10001001, 0b10001001, 0b01110110,               // 8    0x38
  0b01110010, 0b10001001, 0b10001001, 0b01111110,               // 9    0x39
  0b00100010,                                                   // :    0x3A
  0b00000001, 0b00100010,                                       // ;    0x3B
  0b00011000, 0b00100100, 0b01000010, 0b10000001,               // <    0x3C
  0b00010100, 0b00010100, 0b00010100, 0b00010100,               // =    0x3D
  0b10000001, 0b01000010, 0b00100100, 0b00011000,               // >    0x3E
  0b01000000, 0b10001101, 0b10001000, 0b01110000,               // ?    0x3F
  0b01111110, 0b10000001, 0b10111001, 0b10000101, 0b01111100,   // @    0x40
  0b01111111, 0b10001000, 0b10001000, 0b01111111,               // A    0x41
  0b11111111, 0b10001001, 0b10001001, 0b01110110,               // B    0x42
  0b01111110, 0b10000001, 0b10000001, 0b01000010,               // C    0x43
  0b11111111, 0b10000001, 0b10000001, 0b01111110,               // D    0x44
  0b11111111, 0b10001001, 0b10001001, 0b10000001,               // E    0x45
  0b11111111, 0b10010000, 0b10010000, 0b10000000,               // F    0x46
  0b01111110, 0b10000001, 0b10001001, 0b01001110,               // G    0x47
  0b11111111, 0b00001000, 0b00001000, 0b11111111,               // H    0x48
  0b10000001, 0b11111111, 0b10000001,                           // I    0x49
  0b10000110, 0b10000001, 0b10000001, 0b11111110,               // J    0x4A
  0b11111111, 0b00010000, 0b00101000, 0b11000111,               // K    0x4B
  0b11111111, 0b00000001, 0b00000001, 0b00000001,               // L    0x4C
  0b01111111, 0b11000000, 0b00110000, 0b11000000, 0b01111111,   // M    0x4D
  0b11111111, 0b01100000, 0b00011000, 0b00000110, 0b11111111,   // N    0x4E
  0b01111110, 0b10000001, 0b10000001, 0b01111110,               // O    0x4F
  0b11111111, 0b10001000, 0b10001000, 0b01110000,               // P    0x50
  0b01111110, 0b10000001, 0b10000101, 0b10000010, 0b01111101,   // Q    0x51
  0b11111111, 0b10001000, 0b10001100, 0b01110011,               // R    0x52
  0b01100010, 0b10010001, 0b10001001, 0b01000110,               // S    0x53
  0b10000000, 0b11111111, 0b10000000,                           // T    0x54
  0b11111110, 0b00000001, 0b00000001, 0b11111110,               // U    0x55
  0b11111110, 0b00000001, 0b00000110, 0b11111000,               // V    0x56
  0b11111100, 0b00000011, 0b00011100, 0b00000011, 0b11111100,   // W    0x57
  0b10000001, 0b01100110, 0b00011000, 0b01100110, 0b10000001,   // X    0x58
  0b11000000, 0b00110000, 0b00001111, 0b00110000, 0b11000000,   // Y    0x59
  0b10000011, 0b10001101, 0b10110001, 0b11000001,               // Z    0x5A
  0b11111111, 0b10000001,                                       // [    0x5B
  0b11000000, 0b00110000, 0b00001100, 0b00000011,               // \    0x5C
  0b10000001, 0b11111111,                                       // ]    0x5D
  0b01000000, 0b10000000, 0b01000000,                           // ^    0x5E
  0b00000001, 0b00000001, 0b00000001, 0b00000001,               // _    0x5F
};

int FONT_8X4_END[] = 
{
  1,   4,   7,  12,  17,  22,  26,  27,
  30,  33,  36,  41,  43,  46,  47,  51,
  55,  58,  62,  66,  70,  74,  78,  82,
  86,  90,  91,  93,  97, 101, 105, 109,
  114, 118, 122, 126, 130, 134, 138, 142,
  146, 149, 153, 157, 161, 166, 171, 175,
  179, 184, 188, 192, 195, 199, 203, 208,
  213, 218, 222, 224, 228, 230, 233, 237
};


//Prototipuri de functii aici
void setup(void);
void SetHT1632CAs3208(void);
void CommandWrite(unsigned int);
void Print(void);
void AddressWrite(unsigned char);
void LEDArrayInitialize(void);
void RefreshLEDArray(void);

//variabile globale
unsigned char LEDArray[32];
String text="R A Z V A N  ";

//definiri de functii
void LEDArrayInitialize(void)
{
  for(int i=0;i<32;i++)
    LEDArray[i]=0x00;
   
}
void SetHT1632CAs3208(void)
{
  CommandWrite(SYS_EN);
  CommandWrite(LED_ON);
  CommandWrite(RC_MASTER_MODE);
  CommandWrite(N_MOS_COM8);
  CommandWrite(PWM_16);
}
void CommandWrite(unsigned int command)
{
  unsigned char i;
  unsigned int j;
  command=command&0x0FFF;//12 bit comm word
  digitalWrite(CS,1);//cs is OFF
  delayMicroseconds(uTIME);
  digitalWrite(CS,0);//cs is OFF
  delayMicroseconds(uTIME);
  for(i=0;i<12;i++)
  {
    digitalWrite(WR,0);
    delayMicroseconds(uTIME);
    
    j=command&0x0800;//return the mMSB
    command=command<<1;
    j=j>>11;
    digitalWrite(DATA,j);
    delayMicroseconds(uTIME);
    
    digitalWrite(WR,1);
    delayMicroseconds(uTIME);
    
  }
  digitalWrite(CS,1);//cs is OFF
}
void AddressWrite(unsigned char address)
{
  unsigned char i, temp;
  address=address&0x7F;
  
  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,1);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,0);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  digitalWrite(WR,0);
  delayMicroseconds(uTIME);
  digitalWrite(DATA,1);
  delayMicroseconds(uTIME);
  digitalWrite(WR,1);
  delayMicroseconds(uTIME);

  for(i=0;i<7;i++)
  {
    digitalWrite(WR,0);
    delayMicroseconds(uTIME);  
    temp=address&0x40;
    address=address<<1;
    temp=temp>>6;
    digitalWrite(DATA,temp);
    delayMicroseconds(uTIME);  
    digitalWrite(WR,1);
    delayMicroseconds(uTIME);  
    
  }

  
}
void Print(void)
{
  unsigned char i=0,j=0;
  bool data;
  AddressWrite(0x00);
  for(i=0;i<32;i++)
  {
     for(j=0;j<8;j++)
     {
          data=(LEDArray[i]&(1<<(7-j)))>>(7-j);
          digitalWrite(WR,0);
          delayMicroseconds(uTIME);
          digitalWrite(DATA,data);
          delayMicroseconds(uTIME);
          digitalWrite(WR,1);
          delayMicroseconds(uTIME);
     }
  }
}
void RefreshLEDArray(void)
{
  unsigned char pos=0;
  for(int i=0;i<text.length();i++)
  {
    text[i]=text[i]-0x20;
    if(text[i]==0x00)
    {
      LEDArray[pos]=FONT[0];
      pos=pos+1;
    }
    else
      for(int j=FONT_8X4_END[text[i]-1];j<FONT_8X4_END[text[i]];j++)
      {
        LEDArray[pos]=FONT[j];
        pos=pos+1;
      }
      
  }
}
void setup(void) 
{
  LEDArrayInitialize();
  pinMode(CS, OUTPUT);
  pinMode(WR, OUTPUT);
  pinMode(DATA, OUTPUT);
  pinMode(13,OUTPUT);
  pinMode(A0, INPUT);
  digitalWrite(13,HIGH);
  SetHT1632CAs3208();
  RefreshLEDArray();

  
}
unsigned char count=0;

void loop() 
{

  digitalWrite(CS,0);
  Print();
  digitalWrite(CS,1);
  delay(500);
  
}
