#include<font_8x4.h>
#include<font_5x4.h>
#include<HT1632.h>
//#define uri aici


//Prototipuri de functii aici
void setup(void);
void Print(void);
void AddressWrite(unsigned char);
void RefreshLEDArray(bool);

//variabile globale
unsigned char LEDArray[32],LEDMessage[400];
String text="B U N A!  E U S U N T  R A Z V A N ! ! ";

//definiri de functii
void LEDArrayInitialize(void)
{
  for(int i=0;i<32;i++)
    LEDArray[i]=0x00;  
}
void Print(void)
{
  unsigned char i=0,j=0;
  bool data;
  AddressWrite(0x00);
  for(i=0;i<32;i++)
  {
     for(j=0;j<8;j++)
     {
          data=(LEDArray[i]&(1<<(7-j)))>>(7-j);
          digitalWrite(WR,0);
          delayMicroseconds(uTIME);
          digitalWrite(DATA,data);
          delayMicroseconds(uTIME);
          digitalWrite(WR,1);
          delayMicroseconds(uTIME);
     }
  }
}
unsigned char first=0;
void RefreshLEDArray()
{
  LEDArrayInitialize();
  for(int i=first;i<32+first;i++)
    LEDArray[i-first]=LEDMessage[i];

}
void ConvertText(bool font)//1-> 5*4 FONT   0->8*4 FONT
{
  unsigned char pos=0;
  if(!font)
  {
    for(int i=0;i<text.length();i++)
    {
      text[i]=text[i]-0x20;
      if(text[i]==0x00)
      {
        LEDMessage[pos]=FONT_8X4[0];
        pos=pos+1;
      }
      else
        for(int j=FONT_8X4_END[text[i]-1];j<FONT_8X4_END[text[i]];j++)
        {
          LEDMessage[pos]=FONT_8X4[j];
          pos=pos+1;
        }   
    }
  }
  else
  {
    for(int i=0;i<text.length();i++)
    {
      text[i]=text[i]-0x20;
      if(text[i]==0x00)
      {
        LEDMessage[pos]=FONT_5X4[0];
        pos=pos+1;
      }
      else
        for(int j=FONT_5X4_END[text[i]-1];j<FONT_5X4_END[text[i]];j++)
        {
          LEDMessage[pos]=FONT_5X4[j]>>2;
          pos=pos+1;
        }   
    }
  }
  
}
void setup(void) 
{
  pinMode(CS, OUTPUT);
  pinMode(WR, OUTPUT);
  pinMode(DATA, OUTPUT);
  pinMode(13,OUTPUT);
  pinMode(A0, INPUT);
  
  SetHT1632CAs3208();
  LEDArrayInitialize();
  ConvertText(1);//converteste textul in cod in LEDMEssage
  RefreshLEDArray();
  
}
unsigned char count=0;

void loop() 
{
  RefreshLEDArray();
  first++;
  digitalWrite(CS,0);
  Print();
  digitalWrite(CS,1);
  delay(700);
  
}
